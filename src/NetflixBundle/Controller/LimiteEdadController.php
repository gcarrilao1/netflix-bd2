<?php

namespace NetflixBundle\Controller;

use NetflixBundle\Entity\LimiteEdad;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Limiteedad controller.
 *
 */
class LimiteEdadController extends Controller
{
    /**
     * Lists all limiteEdad entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $limiteEdads = $em->getRepository('NetflixBundle:LimiteEdad')->findAll();

        return $this->render('limiteedad/index.html.twig', array(
            'limiteEdads' => $limiteEdads,
        ));
    }

    /**
     * Creates a new limiteEdad entity.
     *
     */
    public function newAction(Request $request)
    {
        $limiteEdad = new Limiteedad();
        $form = $this->createForm('NetflixBundle\Form\LimiteEdadType', $limiteEdad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($limiteEdad);
            $em->flush();

            return $this->redirectToRoute('limiteedad_show', array('id' => $limiteEdad->getId()));
        }

        return $this->render('limiteedad/new.html.twig', array(
            'limiteEdad' => $limiteEdad,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a limiteEdad entity.
     *
     */
    public function showAction(LimiteEdad $limiteEdad)
    {
        $deleteForm = $this->createDeleteForm($limiteEdad);

        return $this->render('limiteedad/show.html.twig', array(
            'limiteEdad' => $limiteEdad,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing limiteEdad entity.
     *
     */
    public function editAction(Request $request, LimiteEdad $limiteEdad)
    {
        $deleteForm = $this->createDeleteForm($limiteEdad);
        $editForm = $this->createForm('NetflixBundle\Form\LimiteEdadType', $limiteEdad);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('limiteedad_edit', array('id' => $limiteEdad->getId()));
        }

        return $this->render('limiteedad/edit.html.twig', array(
            'limiteEdad' => $limiteEdad,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a limiteEdad entity.
     *
     */
    public function deleteAction(Request $request, LimiteEdad $limiteEdad)
    {
        $form = $this->createDeleteForm($limiteEdad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($limiteEdad);
            $em->flush();
        }

        return $this->redirectToRoute('limiteedad_index');
    }

    /**
     * Creates a form to delete a limiteEdad entity.
     *
     * @param LimiteEdad $limiteEdad The limiteEdad entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(LimiteEdad $limiteEdad)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('limiteedad_delete', array('id' => $limiteEdad->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
