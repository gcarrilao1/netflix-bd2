<?php

namespace NetflixBundle\Controller;

use NetflixBundle\Entity\Idioma;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Idioma controller.
 *
 */
class IdiomaController extends Controller
{
    /**
     * Lists all idioma entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $idiomas = $em->getRepository('NetflixBundle:Idioma')->findAll();

        return $this->render('idioma/index.html.twig', array(
            'idiomas' => $idiomas,
        ));
    }

    /**
     * Creates a new idioma entity.
     *
     */
    public function newAction(Request $request)
    {
        $idioma = new Idioma();
        $form = $this->createForm('NetflixBundle\Form\IdiomaType', $idioma);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($idioma);
            $em->flush();

            return $this->redirectToRoute('idioma_show', array('id' => $idioma->getId()));
        }

        return $this->render('idioma/new.html.twig', array(
            'idioma' => $idioma,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a idioma entity.
     *
     */
    public function showAction(Idioma $idioma)
    {
        $deleteForm = $this->createDeleteForm($idioma);

        return $this->render('idioma/show.html.twig', array(
            'idioma' => $idioma,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing idioma entity.
     *
     */
    public function editAction(Request $request, Idioma $idioma)
    {
        $deleteForm = $this->createDeleteForm($idioma);
        $editForm = $this->createForm('NetflixBundle\Form\IdiomaType', $idioma);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('idioma_edit', array('id' => $idioma->getId()));
        }

        return $this->render('idioma/edit.html.twig', array(
            'idioma' => $idioma,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a idioma entity.
     *
     */
    public function deleteAction(Request $request, Idioma $idioma)
    {
        $form = $this->createDeleteForm($idioma);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($idioma);
            $em->flush();
        }

        return $this->redirectToRoute('idioma_index');
    }

    /**
     * Creates a form to delete a idioma entity.
     *
     * @param Idioma $idioma The idioma entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Idioma $idioma)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('idioma_delete', array('id' => $idioma->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
