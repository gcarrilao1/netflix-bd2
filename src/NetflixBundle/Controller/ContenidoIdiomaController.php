<?php

namespace NetflixBundle\Controller;

use NetflixBundle\Entity\ContenidoIdioma;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Contenidoidioma controller.
 *
 */
class ContenidoIdiomaController extends Controller
{
    /**
     * Lists all contenidoIdioma entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $contenidoIdiomas = $em->getRepository('NetflixBundle:ContenidoIdioma')->findAll();

        return $this->render('contenidoidioma/index.html.twig', array(
            'contenidoIdiomas' => $contenidoIdiomas,
        ));
    }

    /**
     * Creates a new contenidoIdioma entity.
     *
     */
    public function newAction(Request $request)
    {
        $contenidoIdioma = new Contenidoidioma();
        $form = $this->createForm('NetflixBundle\Form\ContenidoIdiomaType', $contenidoIdioma);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contenidoIdioma);
            $em->flush();

            return $this->redirectToRoute('contenidoidioma_show', array('id' => $contenidoIdioma->getId()));
        }

        return $this->render('contenidoidioma/new.html.twig', array(
            'contenidoIdioma' => $contenidoIdioma,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a contenidoIdioma entity.
     *
     */
    public function showAction(ContenidoIdioma $contenidoIdioma)
    {
        $deleteForm = $this->createDeleteForm($contenidoIdioma);

        return $this->render('contenidoidioma/show.html.twig', array(
            'contenidoIdioma' => $contenidoIdioma,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing contenidoIdioma entity.
     *
     */
    public function editAction(Request $request, ContenidoIdioma $contenidoIdioma)
    {
        $deleteForm = $this->createDeleteForm($contenidoIdioma);
        $editForm = $this->createForm('NetflixBundle\Form\ContenidoIdiomaType', $contenidoIdioma);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('contenidoidioma_edit', array('id' => $contenidoIdioma->getId()));
        }

        return $this->render('contenidoidioma/edit.html.twig', array(
            'contenidoIdioma' => $contenidoIdioma,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a contenidoIdioma entity.
     *
     */
    public function deleteAction(Request $request, ContenidoIdioma $contenidoIdioma)
    {
        $form = $this->createDeleteForm($contenidoIdioma);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($contenidoIdioma);
            $em->flush();
        }

        return $this->redirectToRoute('contenidoidioma_index');
    }

    /**
     * Creates a form to delete a contenidoIdioma entity.
     *
     * @param ContenidoIdioma $contenidoIdioma The contenidoIdioma entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ContenidoIdioma $contenidoIdioma)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('contenidoidioma_delete', array('id' => $contenidoIdioma->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
