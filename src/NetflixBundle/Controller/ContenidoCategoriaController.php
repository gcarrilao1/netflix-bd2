<?php

namespace NetflixBundle\Controller;

use NetflixBundle\Entity\ContenidoCategoria;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Contenidocategorium controller.
 *
 */
class ContenidoCategoriaController extends Controller
{
    /**
     * Lists all contenidoCategorium entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $contenidoCategorias = $em->getRepository('NetflixBundle:ContenidoCategoria')->findAll();

        return $this->render('contenidocategoria/index.html.twig', array(
            'contenidoCategorias' => $contenidoCategorias,
        ));
    }

    /**
     * Creates a new contenidoCategorium entity.
     *
     */
    public function newAction(Request $request)
    {
        $contenidoCategorium = new Contenidocategorium();
        $form = $this->createForm('NetflixBundle\Form\ContenidoCategoriaType', $contenidoCategorium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contenidoCategorium);
            $em->flush();

            return $this->redirectToRoute('contenidocategoria_show', array('id' => $contenidoCategorium->getId()));
        }

        return $this->render('contenidocategoria/new.html.twig', array(
            'contenidoCategorium' => $contenidoCategorium,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a contenidoCategorium entity.
     *
     */
    public function showAction(ContenidoCategoria $contenidoCategorium)
    {
        $deleteForm = $this->createDeleteForm($contenidoCategorium);

        return $this->render('contenidocategoria/show.html.twig', array(
            'contenidoCategorium' => $contenidoCategorium,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing contenidoCategorium entity.
     *
     */
    public function editAction(Request $request, ContenidoCategoria $contenidoCategorium)
    {
        $deleteForm = $this->createDeleteForm($contenidoCategorium);
        $editForm = $this->createForm('NetflixBundle\Form\ContenidoCategoriaType', $contenidoCategorium);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('contenidocategoria_edit', array('id' => $contenidoCategorium->getId()));
        }

        return $this->render('contenidocategoria/edit.html.twig', array(
            'contenidoCategorium' => $contenidoCategorium,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a contenidoCategorium entity.
     *
     */
    public function deleteAction(Request $request, ContenidoCategoria $contenidoCategorium)
    {
        $form = $this->createDeleteForm($contenidoCategorium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($contenidoCategorium);
            $em->flush();
        }

        return $this->redirectToRoute('contenidocategoria_index');
    }

    /**
     * Creates a form to delete a contenidoCategorium entity.
     *
     * @param ContenidoCategoria $contenidoCategorium The contenidoCategorium entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ContenidoCategoria $contenidoCategorium)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('contenidocategoria_delete', array('id' => $contenidoCategorium->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
