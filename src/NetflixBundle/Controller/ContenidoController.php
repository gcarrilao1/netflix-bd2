<?php

namespace NetflixBundle\Controller;

use NetflixBundle\Entity\Contenido;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Contenido controller.
 *
 */
class ContenidoController extends Controller
{
    /**
     * Lists all contenido entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $contenidos = $em->getRepository('NetflixBundle:Contenido')->findAll();

        return $this->render('contenido/index.html.twig', array(
            'contenidos' => $contenidos,
        ));
    }

    /**
     * Creates a new contenido entity.
     *
     */
    public function newAction(Request $request)
    {
        $contenido = new Contenido();
        $form = $this->createForm('NetflixBundle\Form\ContenidoType', $contenido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contenido);
            $em->flush();

            return $this->redirectToRoute('contenido_show', array('id' => $contenido->getId()));
        }

        return $this->render('contenido/new.html.twig', array(
            'contenido' => $contenido,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a contenido entity.
     *
     */
    public function showAction(Contenido $contenido)
    {
        $deleteForm = $this->createDeleteForm($contenido);

        return $this->render('contenido/show.html.twig', array(
            'contenido' => $contenido,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing contenido entity.
     *
     */
    public function editAction(Request $request, Contenido $contenido)
    {
        $deleteForm = $this->createDeleteForm($contenido);
        $editForm = $this->createForm('NetflixBundle\Form\ContenidoType', $contenido);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('contenido_edit', array('id' => $contenido->getId()));
        }

        return $this->render('contenido/edit.html.twig', array(
            'contenido' => $contenido,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a contenido entity.
     *
     */
    public function deleteAction(Request $request, Contenido $contenido)
    {
        $form = $this->createDeleteForm($contenido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($contenido);
            $em->flush();
        }

        return $this->redirectToRoute('contenido_index');
    }

    /**
     * Creates a form to delete a contenido entity.
     *
     * @param Contenido $contenido The contenido entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Contenido $contenido)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('contenido_delete', array('id' => $contenido->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
