<?php

namespace NetflixBundle\Controller;

use NetflixBundle\Entity\Episodio;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Episodio controller.
 *
 */
class EpisodioController extends Controller
{
    /**
     * Lists all episodio entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $episodios = $em->getRepository('NetflixBundle:Episodio')->findAll();

        return $this->render('episodio/index.html.twig', array(
            'episodios' => $episodios,
        ));
    }

    /**
     * Creates a new episodio entity.
     *
     */
    public function newAction(Request $request)
    {
        $episodio = new Episodio();
        $form = $this->createForm('NetflixBundle\Form\EpisodioType', $episodio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($episodio);
            $em->flush();

            return $this->redirectToRoute('episodio_show', array('id' => $episodio->getId()));
        }

        return $this->render('episodio/new.html.twig', array(
            'episodio' => $episodio,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a episodio entity.
     *
     */
    public function showAction(Episodio $episodio)
    {
        $deleteForm = $this->createDeleteForm($episodio);

        return $this->render('episodio/show.html.twig', array(
            'episodio' => $episodio,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing episodio entity.
     *
     */
    public function editAction(Request $request, Episodio $episodio)
    {
        $deleteForm = $this->createDeleteForm($episodio);
        $editForm = $this->createForm('NetflixBundle\Form\EpisodioType', $episodio);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('episodio_edit', array('id' => $episodio->getId()));
        }

        return $this->render('episodio/edit.html.twig', array(
            'episodio' => $episodio,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a episodio entity.
     *
     */
    public function deleteAction(Request $request, Episodio $episodio)
    {
        $form = $this->createDeleteForm($episodio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($episodio);
            $em->flush();
        }

        return $this->redirectToRoute('episodio_index');
    }

    /**
     * Creates a form to delete a episodio entity.
     *
     * @param Episodio $episodio The episodio entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Episodio $episodio)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('episodio_delete', array('id' => $episodio->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
