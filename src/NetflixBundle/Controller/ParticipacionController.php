<?php

namespace NetflixBundle\Controller;

use NetflixBundle\Entity\Participacion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Participacion controller.
 *
 */
class ParticipacionController extends Controller
{
    /**
     * Lists all participacion entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $participacions = $em->getRepository('NetflixBundle:Participacion')->findAll();

        return $this->render('participacion/index.html.twig', array(
            'participacions' => $participacions,
        ));
    }

    /**
     * Creates a new participacion entity.
     *
     */
    public function newAction(Request $request)
    {
        $participacion = new Participacion();
        $form = $this->createForm('NetflixBundle\Form\ParticipacionType', $participacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($participacion);
            $em->flush();

            return $this->redirectToRoute('participacion_show', array('id' => $participacion->getId()));
        }

        return $this->render('participacion/new.html.twig', array(
            'participacion' => $participacion,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a participacion entity.
     *
     */
    public function showAction(Participacion $participacion)
    {
        $deleteForm = $this->createDeleteForm($participacion);

        return $this->render('participacion/show.html.twig', array(
            'participacion' => $participacion,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing participacion entity.
     *
     */
    public function editAction(Request $request, Participacion $participacion)
    {
        $deleteForm = $this->createDeleteForm($participacion);
        $editForm = $this->createForm('NetflixBundle\Form\ParticipacionType', $participacion);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('participacion_edit', array('id' => $participacion->getId()));
        }

        return $this->render('participacion/edit.html.twig', array(
            'participacion' => $participacion,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a participacion entity.
     *
     */
    public function deleteAction(Request $request, Participacion $participacion)
    {
        $form = $this->createDeleteForm($participacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($participacion);
            $em->flush();
        }

        return $this->redirectToRoute('participacion_index');
    }

    /**
     * Creates a form to delete a participacion entity.
     *
     * @param Participacion $participacion The participacion entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Participacion $participacion)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('participacion_delete', array('id' => $participacion->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
