<?php

namespace NetflixBundle\Controller;

use NetflixBundle\Entity\ConfiguracionReproduccion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Configuracionreproduccion controller.
 *
 */
class ConfiguracionReproduccionController extends Controller
{
    /**
     * Lists all configuracionReproduccion entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $configuracionReproduccions = $em->getRepository('NetflixBundle:ConfiguracionReproduccion')->findAll();

        return $this->render('configuracionreproduccion/index.html.twig', array(
            'configuracionReproduccions' => $configuracionReproduccions,
        ));
    }

    /**
     * Creates a new configuracionReproduccion entity.
     *
     */
    public function newAction(Request $request)
    {
        $configuracionReproduccion = new Configuracionreproduccion();
        $form = $this->createForm('NetflixBundle\Form\ConfiguracionReproduccionType', $configuracionReproduccion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($configuracionReproduccion);
            $em->flush();

            return $this->redirectToRoute('configuracionreproduccion_show', array('id' => $configuracionReproduccion->getId()));
        }

        return $this->render('configuracionreproduccion/new.html.twig', array(
            'configuracionReproduccion' => $configuracionReproduccion,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a configuracionReproduccion entity.
     *
     */
    public function showAction(ConfiguracionReproduccion $configuracionReproduccion)
    {
        $deleteForm = $this->createDeleteForm($configuracionReproduccion);

        return $this->render('configuracionreproduccion/show.html.twig', array(
            'configuracionReproduccion' => $configuracionReproduccion,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing configuracionReproduccion entity.
     *
     */
    public function editAction(Request $request, ConfiguracionReproduccion $configuracionReproduccion)
    {
        $deleteForm = $this->createDeleteForm($configuracionReproduccion);
        $editForm = $this->createForm('NetflixBundle\Form\ConfiguracionReproduccionType', $configuracionReproduccion);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('configuracionreproduccion_edit', array('id' => $configuracionReproduccion->getId()));
        }

        return $this->render('configuracionreproduccion/edit.html.twig', array(
            'configuracionReproduccion' => $configuracionReproduccion,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a configuracionReproduccion entity.
     *
     */
    public function deleteAction(Request $request, ConfiguracionReproduccion $configuracionReproduccion)
    {
        $form = $this->createDeleteForm($configuracionReproduccion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($configuracionReproduccion);
            $em->flush();
        }

        return $this->redirectToRoute('configuracionreproduccion_index');
    }

    /**
     * Creates a form to delete a configuracionReproduccion entity.
     *
     * @param ConfiguracionReproduccion $configuracionReproduccion The configuracionReproduccion entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ConfiguracionReproduccion $configuracionReproduccion)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('configuracionreproduccion_delete', array('id' => $configuracionReproduccion->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
