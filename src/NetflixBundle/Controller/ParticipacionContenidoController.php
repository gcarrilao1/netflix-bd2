<?php

namespace NetflixBundle\Controller;

use NetflixBundle\Entity\ParticipacionContenido;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Participacioncontenido controller.
 *
 */
class ParticipacionContenidoController extends Controller
{
    /**
     * Lists all participacionContenido entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $participacionContenidos = $em->getRepository('NetflixBundle:ParticipacionContenido')->findAll();

        return $this->render('participacioncontenido/index.html.twig', array(
            'participacionContenidos' => $participacionContenidos,
        ));
    }

    /**
     * Creates a new participacionContenido entity.
     *
     */
    public function newAction(Request $request)
    {
        $participacionContenido = new Participacioncontenido();
        $form = $this->createForm('NetflixBundle\Form\ParticipacionContenidoType', $participacionContenido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($participacionContenido);
            $em->flush();

            return $this->redirectToRoute('participacioncontenido_show', array('id' => $participacionContenido->getId()));
        }

        return $this->render('participacioncontenido/new.html.twig', array(
            'participacionContenido' => $participacionContenido,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a participacionContenido entity.
     *
     */
    public function showAction(ParticipacionContenido $participacionContenido)
    {
        $deleteForm = $this->createDeleteForm($participacionContenido);

        return $this->render('participacioncontenido/show.html.twig', array(
            'participacionContenido' => $participacionContenido,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing participacionContenido entity.
     *
     */
    public function editAction(Request $request, ParticipacionContenido $participacionContenido)
    {
        $deleteForm = $this->createDeleteForm($participacionContenido);
        $editForm = $this->createForm('NetflixBundle\Form\ParticipacionContenidoType', $participacionContenido);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('participacioncontenido_edit', array('id' => $participacionContenido->getId()));
        }

        return $this->render('participacioncontenido/edit.html.twig', array(
            'participacionContenido' => $participacionContenido,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a participacionContenido entity.
     *
     */
    public function deleteAction(Request $request, ParticipacionContenido $participacionContenido)
    {
        $form = $this->createDeleteForm($participacionContenido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($participacionContenido);
            $em->flush();
        }

        return $this->redirectToRoute('participacioncontenido_index');
    }

    /**
     * Creates a form to delete a participacionContenido entity.
     *
     * @param ParticipacionContenido $participacionContenido The participacionContenido entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ParticipacionContenido $participacionContenido)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('participacioncontenido_delete', array('id' => $participacionContenido->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
