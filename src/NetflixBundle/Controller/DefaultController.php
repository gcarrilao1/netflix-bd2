<?php

namespace NetflixBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('NetflixBundle:Default:index.html.twig');
    }
}
