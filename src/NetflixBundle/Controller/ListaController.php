<?php

namespace NetflixBundle\Controller;

use NetflixBundle\Entity\Lista;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Listum controller.
 *
 */
class ListaController extends Controller
{
    /**
     * Lists all listum entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $listas = $em->getRepository('NetflixBundle:Lista')->findAll();

        return $this->render('lista/index.html.twig', array(
            'listas' => $listas,
        ));
    }

    /**
     * Creates a new listum entity.
     *
     */
    public function newAction(Request $request)
    {
        $listum = new Listum();
        $form = $this->createForm('NetflixBundle\Form\ListaType', $listum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($listum);
            $em->flush();

            return $this->redirectToRoute('lista_show', array('id' => $listum->getId()));
        }

        return $this->render('lista/new.html.twig', array(
            'listum' => $listum,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a listum entity.
     *
     */
    public function showAction(Lista $listum)
    {
        $deleteForm = $this->createDeleteForm($listum);

        return $this->render('lista/show.html.twig', array(
            'listum' => $listum,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing listum entity.
     *
     */
    public function editAction(Request $request, Lista $listum)
    {
        $deleteForm = $this->createDeleteForm($listum);
        $editForm = $this->createForm('NetflixBundle\Form\ListaType', $listum);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('lista_edit', array('id' => $listum->getId()));
        }

        return $this->render('lista/edit.html.twig', array(
            'listum' => $listum,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a listum entity.
     *
     */
    public function deleteAction(Request $request, Lista $listum)
    {
        $form = $this->createDeleteForm($listum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($listum);
            $em->flush();
        }

        return $this->redirectToRoute('lista_index');
    }

    /**
     * Creates a form to delete a listum entity.
     *
     * @param Lista $listum The listum entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Lista $listum)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('lista_delete', array('id' => $listum->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
