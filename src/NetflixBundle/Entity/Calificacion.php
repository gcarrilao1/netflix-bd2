<?php

namespace NetflixBundle\Entity;

/**
 * Calificacion
 */
class Calificacion
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $calificacion;

    /**
     * @var \NetflixBundle\Entity\Contenido
     */
    private $contenido;

    /**
     * @var \NetflixBundle\Entity\Perfil
     */
    private $perfil;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set calificacion
     *
     * @param integer $calificacion
     *
     * @return Calificacion
     */
    public function setCalificacion($calificacion)
    {
        $this->calificacion = $calificacion;

        return $this;
    }

    /**
     * Get calificacion
     *
     * @return integer
     */
    public function getCalificacion()
    {
        return $this->calificacion;
    }

    /**
     * Set contenido
     *
     * @param \NetflixBundle\Entity\Contenido $contenido
     *
     * @return Calificacion
     */
    public function setContenido(\NetflixBundle\Entity\Contenido $contenido = null)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return \NetflixBundle\Entity\Contenido
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Set perfil
     *
     * @param \NetflixBundle\Entity\Perfil $perfil
     *
     * @return Calificacion
     */
    public function setPerfil(\NetflixBundle\Entity\Perfil $perfil = null)
    {
        $this->perfil = $perfil;

        return $this;
    }

    /**
     * Get perfil
     *
     * @return \NetflixBundle\Entity\Perfil
     */
    public function getPerfil()
    {
        return $this->perfil;
    }
}

