<?php

namespace NetflixBundle\Entity;

/**
 * Usuario
 */
class Usuario
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $apellido;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $tarjetaCredito;

    /**
     * @var \DateTime
     */
    private $fechaIngreso;

    /**
     * @var \NetflixBundle\Entity\Ciudad
     */
    private $ciudad;

    /**
     * @var \NetflixBundle\Entity\Plan
     */
    private $plan;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Usuario
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     *
     * @return Usuario
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Usuario
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set tarjetaCredito
     *
     * @param string $tarjetaCredito
     *
     * @return Usuario
     */
    public function setTarjetaCredito($tarjetaCredito)
    {
        $this->tarjetaCredito = $tarjetaCredito;

        return $this;
    }

    /**
     * Get tarjetaCredito
     *
     * @return string
     */
    public function getTarjetaCredito()
    {
        return $this->tarjetaCredito;
    }

    /**
     * Set fechaIngreso
     *
     * @param \DateTime $fechaIngreso
     *
     * @return Usuario
     */
    public function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    /**
     * Get fechaIngreso
     *
     * @return \DateTime
     */
    public function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    /**
     * Set ciudad
     *
     * @param \NetflixBundle\Entity\Ciudad $ciudad
     *
     * @return Usuario
     */
    public function setCiudad(\NetflixBundle\Entity\Ciudad $ciudad = null)
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    /**
     * Get ciudad
     *
     * @return \NetflixBundle\Entity\Ciudad
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * Set plan
     *
     * @param \NetflixBundle\Entity\Plan $plan
     *
     * @return Usuario
     */
    public function setPlan(\NetflixBundle\Entity\Plan $plan = null)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return \NetflixBundle\Entity\Plan
     */
    public function getPlan()
    {
        return $this->plan;
    }
}

