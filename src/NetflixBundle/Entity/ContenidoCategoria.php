<?php

namespace NetflixBundle\Entity;

/**
 * ContenidoCategoria
 */
class ContenidoCategoria
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \NetflixBundle\Entity\Categoria
     */
    private $categoria;

    /**
     * @var \NetflixBundle\Entity\Contenido
     */
    private $contenido;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoria
     *
     * @param \NetflixBundle\Entity\Categoria $categoria
     *
     * @return ContenidoCategoria
     */
    public function setCategoria(\NetflixBundle\Entity\Categoria $categoria = null)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \NetflixBundle\Entity\Categoria
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set contenido
     *
     * @param \NetflixBundle\Entity\Contenido $contenido
     *
     * @return ContenidoCategoria
     */
    public function setContenido(\NetflixBundle\Entity\Contenido $contenido = null)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return \NetflixBundle\Entity\Contenido
     */
    public function getContenido()
    {
        return $this->contenido;
    }
}

