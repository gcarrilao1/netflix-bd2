<?php

namespace NetflixBundle\Entity;

/**
 * Pelicula
 */
class Pelicula
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $duracion;

    /**
     * @var \NetflixBundle\Entity\Contenido
     */
    private $contenido;


    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Pelicula
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set duracion
     *
     * @param string $duracion
     *
     * @return Pelicula
     */
    public function setDuracion($duracion)
    {
        $this->duracion = $duracion;

        return $this;
    }

    /**
     * Get duracion
     *
     * @return string
     */
    public function getDuracion()
    {
        return $this->duracion;
    }

    /**
     * Set contenido
     *
     * @param \NetflixBundle\Entity\Contenido $contenido
     *
     * @return Pelicula
     */
    public function setContenido(\NetflixBundle\Entity\Contenido $contenido = null)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return \NetflixBundle\Entity\Contenido
     */
    public function getContenido()
    {
        return $this->contenido;
    }
}

