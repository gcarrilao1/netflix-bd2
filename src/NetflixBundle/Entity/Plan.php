<?php

namespace NetflixBundle\Entity;

/**
 * Plan
 */
class Plan
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var integer
     */
    private $perfiles;

    /**
     * @var float
     */
    private $monto;

    /**
     * @var string
     */
    private $calidad;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Plan
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set perfiles
     *
     * @param integer $perfiles
     *
     * @return Plan
     */
    public function setPerfiles($perfiles)
    {
        $this->perfiles = $perfiles;

        return $this;
    }

    /**
     * Get perfiles
     *
     * @return integer
     */
    public function getPerfiles()
    {
        return $this->perfiles;
    }

    /**
     * Set monto
     *
     * @param float $monto
     *
     * @return Plan
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return float
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * Set calidad
     *
     * @param string $calidad
     *
     * @return Plan
     */
    public function setCalidad($calidad)
    {
        $this->calidad = $calidad;

        return $this;
    }

    /**
     * Get calidad
     *
     * @return string
     */
    public function getCalidad()
    {
        return $this->calidad;
    }
}

