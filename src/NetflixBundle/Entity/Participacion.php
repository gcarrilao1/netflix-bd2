<?php

namespace NetflixBundle\Entity;

/**
 * Participacion
 */
class Participacion
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \NetflixBundle\Entity\Persona
     */
    private $persona;

    /**
     * @var \NetflixBundle\Entity\Rol
     */
    private $rol;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set persona
     *
     * @param \NetflixBundle\Entity\Persona $persona
     *
     * @return Participacion
     */
    public function setPersona(\NetflixBundle\Entity\Persona $persona = null)
    {
        $this->persona = $persona;

        return $this;
    }

    /**
     * Get persona
     *
     * @return \NetflixBundle\Entity\Persona
     */
    public function getPersona()
    {
        return $this->persona;
    }

    /**
     * Set rol
     *
     * @param \NetflixBundle\Entity\Rol $rol
     *
     * @return Participacion
     */
    public function setRol(\NetflixBundle\Entity\Rol $rol = null)
    {
        $this->rol = $rol;

        return $this;
    }

    /**
     * Get rol
     *
     * @return \NetflixBundle\Entity\Rol
     */
    public function getRol()
    {
        return $this->rol;
    }
}

