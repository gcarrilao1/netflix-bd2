<?php

namespace NetflixBundle\Entity;

/**
 * ParticipacionContenido
 */
class ParticipacionContenido
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \NetflixBundle\Entity\Contenido
     */
    private $contenido;

    /**
     * @var \NetflixBundle\Entity\Participacion
     */
    private $participacion;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contenido
     *
     * @param \NetflixBundle\Entity\Contenido $contenido
     *
     * @return ParticipacionContenido
     */
    public function setContenido(\NetflixBundle\Entity\Contenido $contenido = null)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return \NetflixBundle\Entity\Contenido
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Set participacion
     *
     * @param \NetflixBundle\Entity\Participacion $participacion
     *
     * @return ParticipacionContenido
     */
    public function setParticipacion(\NetflixBundle\Entity\Participacion $participacion = null)
    {
        $this->participacion = $participacion;

        return $this;
    }

    /**
     * Get participacion
     *
     * @return \NetflixBundle\Entity\Participacion
     */
    public function getParticipacion()
    {
        return $this->participacion;
    }
}

