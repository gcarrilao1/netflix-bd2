<?php

namespace NetflixBundle\Entity;

/**
 * Ciudad
 */
class Ciudad
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var \NetflixBundle\Entity\Pais
     */
    private $pais;


    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Ciudad
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Ciudad
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set pais
     *
     * @param \NetflixBundle\Entity\Pais $pais
     *
     * @return Ciudad
     */
    public function setPais(\NetflixBundle\Entity\Pais $pais = null)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return \NetflixBundle\Entity\Pais
     */
    public function getPais()
    {
        return $this->pais;
    }
}

