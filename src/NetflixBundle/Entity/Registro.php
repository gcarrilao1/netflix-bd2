<?php

namespace NetflixBundle\Entity;

/**
 * Registro
 */
class Registro
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $fecha;

    /**
     * @var \NetflixBundle\Entity\Contenido
     */
    private $contenido;

    /**
     * @var \NetflixBundle\Entity\Perfil
     */
    private $perfil;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Registro
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set contenido
     *
     * @param \NetflixBundle\Entity\Contenido $contenido
     *
     * @return Registro
     */
    public function setContenido(\NetflixBundle\Entity\Contenido $contenido = null)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return \NetflixBundle\Entity\Contenido
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Set perfil
     *
     * @param \NetflixBundle\Entity\Perfil $perfil
     *
     * @return Registro
     */
    public function setPerfil(\NetflixBundle\Entity\Perfil $perfil = null)
    {
        $this->perfil = $perfil;

        return $this;
    }

    /**
     * Get perfil
     *
     * @return \NetflixBundle\Entity\Perfil
     */
    public function getPerfil()
    {
        return $this->perfil;
    }
}

