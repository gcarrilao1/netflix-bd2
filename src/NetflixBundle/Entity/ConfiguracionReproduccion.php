<?php

namespace NetflixBundle\Entity;

/**
 * ConfiguracionReproduccion
 */
class ConfiguracionReproduccion
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $datos;

    /**
     * @var string
     */
    private $descripcion;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datos
     *
     * @param string $datos
     *
     * @return ConfiguracionReproduccion
     */
    public function setDatos($datos)
    {
        $this->datos = $datos;

        return $this;
    }

    /**
     * Get datos
     *
     * @return string
     */
    public function getDatos()
    {
        return $this->datos;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return ConfiguracionReproduccion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
}

