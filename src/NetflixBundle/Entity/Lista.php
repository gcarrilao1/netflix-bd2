<?php

namespace NetflixBundle\Entity;

/**
 * Lista
 */
class Lista
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \NetflixBundle\Entity\Contenido
     */
    private $contenido;

    /**
     * @var \NetflixBundle\Entity\Perfil
     */
    private $perfil;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contenido
     *
     * @param \NetflixBundle\Entity\Contenido $contenido
     *
     * @return Lista
     */
    public function setContenido(\NetflixBundle\Entity\Contenido $contenido = null)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return \NetflixBundle\Entity\Contenido
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Set perfil
     *
     * @param \NetflixBundle\Entity\Perfil $perfil
     *
     * @return Lista
     */
    public function setPerfil(\NetflixBundle\Entity\Perfil $perfil = null)
    {
        $this->perfil = $perfil;

        return $this;
    }

    /**
     * Get perfil
     *
     * @return \NetflixBundle\Entity\Perfil
     */
    public function getPerfil()
    {
        return $this->perfil;
    }
}

