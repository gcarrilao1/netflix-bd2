<?php

namespace NetflixBundle\Entity;

/**
 * ContenidoIdioma
 */
class ContenidoIdioma
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \NetflixBundle\Entity\Contenido
     */
    private $contenido;

    /**
     * @var \NetflixBundle\Entity\Idioma
     */
    private $idioma;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contenido
     *
     * @param \NetflixBundle\Entity\Contenido $contenido
     *
     * @return ContenidoIdioma
     */
    public function setContenido(\NetflixBundle\Entity\Contenido $contenido = null)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return \NetflixBundle\Entity\Contenido
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Set idioma
     *
     * @param \NetflixBundle\Entity\Idioma $idioma
     *
     * @return ContenidoIdioma
     */
    public function setIdioma(\NetflixBundle\Entity\Idioma $idioma = null)
    {
        $this->idioma = $idioma;

        return $this;
    }

    /**
     * Get idioma
     *
     * @return \NetflixBundle\Entity\Idioma
     */
    public function getIdioma()
    {
        return $this->idioma;
    }
}

