<?php

namespace NetflixBundle\Entity;

/**
 * Serie
 */
class Serie
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $episodios;

    /**
     * @var \NetflixBundle\Entity\Contenido
     */
    private $contenido;


    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Serie
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set episodios
     *
     * @param integer $episodios
     *
     * @return Serie
     */
    public function setEpisodios($episodios)
    {
        $this->episodios = $episodios;

        return $this;
    }

    /**
     * Get episodios
     *
     * @return integer
     */
    public function getEpisodios()
    {
        return $this->episodios;
    }

    /**
     * Set contenido
     *
     * @param \NetflixBundle\Entity\Contenido $contenido
     *
     * @return Serie
     */
    public function setContenido(\NetflixBundle\Entity\Contenido $contenido = null)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return \NetflixBundle\Entity\Contenido
     */
    public function getContenido()
    {
        return $this->contenido;
    }
}

