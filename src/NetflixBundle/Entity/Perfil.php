<?php

namespace NetflixBundle\Entity;

/**
 * Perfil
 */
class Perfil
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var integer
     */
    private $listaId;

    /**
     * @var \NetflixBundle\Entity\Usuario
     */
    private $usuario;

    /**
     * @var \NetflixBundle\Entity\ConfiguracionReproduccion
     */
    private $configuracionReproduccion;

    /**
     * @var \NetflixBundle\Entity\Idioma
     */
    private $idioma;

    /**
     * @var \NetflixBundle\Entity\LimiteEdad
     */
    private $limiteEdad;


    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Perfil
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Perfil
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set listaId
     *
     * @param integer $listaId
     *
     * @return Perfil
     */
    public function setListaId($listaId)
    {
        $this->listaId = $listaId;

        return $this;
    }

    /**
     * Get listaId
     *
     * @return integer
     */
    public function getListaId()
    {
        return $this->listaId;
    }

    /**
     * Set usuario
     *
     * @param \NetflixBundle\Entity\Usuario $usuario
     *
     * @return Perfil
     */
    public function setUsuario(\NetflixBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \NetflixBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set configuracionReproduccion
     *
     * @param \NetflixBundle\Entity\ConfiguracionReproduccion $configuracionReproduccion
     *
     * @return Perfil
     */
    public function setConfiguracionReproduccion(\NetflixBundle\Entity\ConfiguracionReproduccion $configuracionReproduccion = null)
    {
        $this->configuracionReproduccion = $configuracionReproduccion;

        return $this;
    }

    /**
     * Get configuracionReproduccion
     *
     * @return \NetflixBundle\Entity\ConfiguracionReproduccion
     */
    public function getConfiguracionReproduccion()
    {
        return $this->configuracionReproduccion;
    }

    /**
     * Set idioma
     *
     * @param \NetflixBundle\Entity\Idioma $idioma
     *
     * @return Perfil
     */
    public function setIdioma(\NetflixBundle\Entity\Idioma $idioma = null)
    {
        $this->idioma = $idioma;

        return $this;
    }

    /**
     * Get idioma
     *
     * @return \NetflixBundle\Entity\Idioma
     */
    public function getIdioma()
    {
        return $this->idioma;
    }

    /**
     * Set limiteEdad
     *
     * @param \NetflixBundle\Entity\LimiteEdad $limiteEdad
     *
     * @return Perfil
     */
    public function setLimiteEdad(\NetflixBundle\Entity\LimiteEdad $limiteEdad = null)
    {
        $this->limiteEdad = $limiteEdad;

        return $this;
    }

    /**
     * Get limiteEdad
     *
     * @return \NetflixBundle\Entity\LimiteEdad
     */
    public function getLimiteEdad()
    {
        return $this->limiteEdad;
    }
}

