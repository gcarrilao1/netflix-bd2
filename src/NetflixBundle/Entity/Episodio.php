<?php

namespace NetflixBundle\Entity;

/**
 * Episodio
 */
class Episodio
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var string
     */
    private $duracion;

    /**
     * @var \NetflixBundle\Entity\Serie
     */
    private $serie;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Episodio
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set duracion
     *
     * @param string $duracion
     *
     * @return Episodio
     */
    public function setDuracion($duracion)
    {
        $this->duracion = $duracion;

        return $this;
    }

    /**
     * Get duracion
     *
     * @return string
     */
    public function getDuracion()
    {
        return $this->duracion;
    }

    /**
     * Set serie
     *
     * @param \NetflixBundle\Entity\Serie $serie
     *
     * @return Episodio
     */
    public function setSerie(\NetflixBundle\Entity\Serie $serie = null)
    {
        $this->serie = $serie;

        return $this;
    }

    /**
     * Get serie
     *
     * @return \NetflixBundle\Entity\Serie
     */
    public function getSerie()
    {
        return $this->serie;
    }
}

